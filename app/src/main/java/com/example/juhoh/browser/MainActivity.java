package com.example.juhoh.browser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText addressBar;
    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addressBar = findViewById(R.id.addressBar);
        web = findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);

        addressBar.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (addressBar.getText().toString().equals("index.html")) {
                        web.loadUrl("file:///android_asset/index.html");
                    }
                    else
                        web.loadUrl(addressBar.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    public void refresh(View v) {
        web.loadUrl("javascript:window.location.reload(true)");
    }

    public void initialize(View v) {
        web.evaluateJavascript("javascript:initialize()", null);
    }

    public void shoutOut(View v) {
        web.evaluateJavascript("javascript:shoutOut()", null);
    }
}
